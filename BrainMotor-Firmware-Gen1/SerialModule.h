
#pragma once

#include <stdint.h>

#include "TaskBase.h"

#include "BocpDefinitions.h"

#include "SerialReader.h"
#include "SerialProcessor.h"
#include "SerialSender.h"

class Scheduler;

class SerialModule : public TaskBase
{
public:
    SerialModule(Scheduler *pScheduler);
    virtual ~SerialModule();

public:
    virtual void RunQuantum();
private:
    void ProcessPacket();
    bool ProcessSYN();
    bool IsValidSYN();
    bool ProcessFIN();
    bool IsValidFIN();
    bool ProcessRTR();
    bool IsValidRTR();
    bool ProcessNormal();

private:
    Scheduler *m_pScheduler;
    SerialReader m_reader;
    SerialProcessor m_processor;
    SerialSender m_sender;

    int m_state;
    bool m_connectedToHost;
    unsigned long m_lastReceivedTimestamp;

    byte m_seqNum;

    byte m_bufDataSeqNum;
    byte m_buf[BOCP_MAX_PAYLOAD_SIZE];
    size_t m_bufDataLength;
};

