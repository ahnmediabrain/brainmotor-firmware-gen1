
#pragma once

#include <stdint.h>

class COBSEncoder
{
public:
    COBSEncoder();
    ~COBSEncoder();

public:
    void Reset(); // 현재 설정된 버퍼가 없는 것으로 내부 상태를 설정
    bool SetInputBuffer(const void *input, size_t length); // 새로운 버퍼를 설정. 이전에 있던 버퍼는 무시됨. 성공 시 true 반환. 실패 시 false 반환.
    int GetNextByte(); // 인코딩이 끝나서 더 이상 반환할 데이터가 없으면 -1이 반환됨
    bool IsEncodingComplete();

private:
    uint8_t m_state;

    const byte *m_inputBase;
    size_t m_length;

    size_t m_bufPos;
    size_t m_thisTimeCopyingBytes;
    size_t m_thisTimeCopiedBytes;
};

