
#include <Arduino.h>
#include "LedModule.h"

#define BOOL2HL(x) ((x) ? HIGH : LOW)

#define PIN_LED_ARDUINO_POWER_PLUS 47
#define PIN_LED_ARDUINO_POWER_MINUS 46
#define PIN_LED_MOTOR_POWER_PLUS 49
#define PIN_LED_MOTOR_POWER_MINUS 48
#define PIN_LED_OSCILLATION_STATE_PLUS 51
#define PIN_LED_OSCILLATION_STATE_MINUS 50
#define PIN_LED_HOST_CONNECTION_PLUS 53
#define PIN_LED_HOST_CONNECTION_MINUS 52

uint16_t LedModule::m_led = 0;

void LedModule::Initialize()
{
    pinMode(PIN_LED_ARDUINO_POWER_PLUS, OUTPUT);
    pinMode(PIN_LED_ARDUINO_POWER_MINUS, OUTPUT);
    pinMode(PIN_LED_MOTOR_POWER_PLUS, OUTPUT);
    pinMode(PIN_LED_MOTOR_POWER_MINUS, OUTPUT);
    pinMode(PIN_LED_OSCILLATION_STATE_PLUS, OUTPUT);
    pinMode(PIN_LED_OSCILLATION_STATE_MINUS, OUTPUT);
    pinMode(PIN_LED_HOST_CONNECTION_PLUS, OUTPUT);
    pinMode(PIN_LED_HOST_CONNECTION_MINUS, OUTPUT);
    UpdatePinVoltage();
}

void LedModule::On(uint16_t pin)
{
    m_led |= pin;
    UpdatePinVoltage();
}
void LedModule::Off(uint16_t pin)
{
    m_led &= ~pin;
    UpdatePinVoltage();
}
void LedModule::UpdatePinVoltage()
{
    digitalWrite(PIN_LED_ARDUINO_POWER_PLUS, BOOL2HL(m_led & LED_ARDUINO_POWER));
    digitalWrite(PIN_LED_MOTOR_POWER_PLUS, BOOL2HL(m_led & LED_MOTOR_POWER));
    digitalWrite(PIN_LED_OSCILLATION_STATE_PLUS, BOOL2HL(m_led & LED_OSCILLATION_STATE));
    digitalWrite(PIN_LED_HOST_CONNECTION_PLUS, BOOL2HL(m_led & LED_HOST_CONNECTION));

    digitalWrite(PIN_LED_ARDUINO_POWER_MINUS, LOW);
    digitalWrite(PIN_LED_MOTOR_POWER_MINUS, LOW);
    digitalWrite(PIN_LED_OSCILLATION_STATE_MINUS, LOW);
    digitalWrite(PIN_LED_HOST_CONNECTION_MINUS, LOW);
}

