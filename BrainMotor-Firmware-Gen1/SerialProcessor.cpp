
#include <Arduino.h>
#include "SerialProcessor.h"

#include "BrainMotor-Firmware-Gen1.h"
#include "Scheduler.h"
#include "Time.h"
#include "DeviceSpec.h"
#include "EndianUtility.h"
#include "OscillationController.h"
#include "InternalStorage.h"

SerialProcessor::SerialProcessor()
{
}
SerialProcessor::~SerialProcessor()
{
}

void SerialProcessor::ProcessMessage(const void *in_void, size_t len, void *out_void, size_t *written)
{
    const byte *in = (const byte *)in_void;
    byte *out = (byte *)out_void;

    //Serial.print("R");

    // NOTE: 길이가 0인 요청은 패킷 프레임 차원에서 필터링하므로, 여기서는 len >= 1이 보장됨
    byte msgType = in[0];
    switch (msgType)
    {
        case BOCP_MSG_ECHO_1:
            ProcessEcho1(in, len, out, written);
            break;
        case BOCP_MSG_TIME_SYNC_1:
            ProcessTimeSync1(in, len, out, written);
            break;
        case BOCP_MSG_SPEC_QUERY_1:
            ProcessSpecQuery1(in, len, out, written);
            break;
        case BOCP_MSG_STATE_QUERY_1:
            ProcessStateQuery1(in, len, out, written);
            break;
        case BOCP_MSG_SETTING_QUERY_1:
            ProcessSettingQuery1(in, len, out, written);
            break;
        case BOCP_MSG_SETTING_UPDATE_1:
            ProcessSettingUpdate1(in, len, out, written);
            break;
        case BOCP_MSG_FINE_CONTROL_1:
            ProcessFineControl1(in, len, out, written);
            break;
        case BOCP_MSG_CONSTANT_VELOCITY_MOVE_1:
            ProcessConstantVelocityMove1(in, len, out, written);
            break;
        case BOCP_MSG_STOP_OSCILLATION_1:
            ProcessStopOscillation1(in, len, out, written);
            break;
        case BOCP_MSG_PREDEFINED_WAVEFORM_OSCILLATION_MODE_1:
            ProcessPredefinedWaveformOscillationMode1(in, len, out, written);
            break;
        case BOCP_MSG_HDM_PREPARE_1:
            ProcessHdmPrepare1(in, len, out, written);
            break;
        case BOCP_MSG_HDM_INITIAL_BUFFERING_REQ:
            ProcessHdmInitialBufferingReq(in, len, out, written);
            break;
        case BOCP_MSG_HDM_OSC_DATA:
            ProcessHdmOscData(in, len, out, written);
            break;
        case BOCP_MSG_HDM_STOP_1:
            ProcessHdmStop1(in, len, out, written);
            break;
        case BOCP_MSG_CONTROL_MOTOR_POWER_1:
            ProcessControlMotorPower1(in, len, out, written);
            break;
        default:
            WriteCommonErrorResponse(BOCP_E_UNSUPPORTED, msgType, out, written);
            break;
    }
}

void SerialProcessor::ConnectionEstablished()
{
}
void SerialProcessor::ConnectionEnded()
{
}
void SerialProcessor::ConnectionReset()
{
}

void SerialProcessor::WriteCommonErrorResponse(BocpStatusCode statusCode, byte requestMsgType, byte *out, size_t *written)
{
    out[0] = BOCP_MSG_COMMON_ERROR_RESPONSE;
    WriteBigEndian16(&out[1], (uint16_t)statusCode);
    out[3] = requestMsgType;
    *written = 4;
}

void SerialProcessor::ProcessEcho1(const byte *in, size_t len, byte *out, size_t *written)
{
    if (len < 2)
    {
        WriteCommonErrorResponse(BOCP_E_INVALID_REQUEST, BOCP_MSG_ECHO_1, out, written);
        return;
    }

    out[0] = BOCP_MSG_ECHO_2;
    memcpy(out + 1, in + 1, len - 1);
    *written = len;
}
void SerialProcessor::ProcessTimeSync1(const byte *in, size_t len, byte *out, size_t *written)
{
    if (len != 9)
    {
        WriteCommonErrorResponse(BOCP_E_INVALID_REQUEST, BOCP_MSG_TIME_SYNC_1, out, written);
        return;
    }

    MICROSECOND64 microsecond64 = Time::GetCurrentTime();
    out[0] = BOCP_MSG_TIME_SYNC_2;
    WriteBigEndian32(out + 1, microsecond64.upper);
    WriteBigEndian32(out + 5, microsecond64.lower);

    *written = 9;
}
void SerialProcessor::ProcessSpecQuery1(const byte *in, size_t len, byte *out, size_t *written)
{
    if (len != 2)
    {
        WriteCommonErrorResponse(BOCP_E_INVALID_REQUEST, BOCP_MSG_SPEC_QUERY_1, out, written);
        return;
    }

    byte specQueryCode = in[1];
    out[0] = BOCP_MSG_SPEC_QUERY_2;
    out[1] = specQueryCode;
    switch (specQueryCode)
    {
        case 0x00:
            memcpy(out + 2, "AhnMedia BrainMotor BM1-001     ", 32); // ModelName
            InternalStorage::ReadSerialNumber(out + 34); // SerialNumber
            WriteBigEndian32(out + 50, 0); // Capabilities
            *written = 54;
            break;
        case 0x01:
            WriteBigEndian32(out + 2, HDM_TIME_UNIT_NANOSECONDS);
            WriteBigEndian32(out + 6, HDM_BUFFER_CAPACITY);
            *written = 10;
            break;
        default:
            WriteCommonErrorResponse(BOCP_E_UNSUPPORTED, BOCP_MSG_SPEC_QUERY_1, out, written);
            break;
    }
}
void SerialProcessor::ProcessStateQuery1(const byte *in, size_t len, byte *out, size_t *written)
{
    if (len != 2)
    {
        WriteCommonErrorResponse(BOCP_E_INVALID_REQUEST, BOCP_MSG_STATE_QUERY_1, out, written);
        return;
    }

    byte stateQueryCode = in[1];
    out[0] = BOCP_MSG_STATE_QUERY_2;
    out[1] = stateQueryCode;
    switch (stateQueryCode)
    {
        case 0x00:
        {
            OscillationController *pOscCtl = g_scheduler.GetOscillationController();
            out[2] = (byte)pOscCtl->GetCurrentOscillationMode();
            out[3] = pOscCtl->IsMotorPoweredOn() ? MOTOR_POWER_ON : MOTOR_POWER_OFF;
            *written = 4;
            break;
        }
        default:
            WriteCommonErrorResponse(BOCP_E_UNSUPPORTED, BOCP_MSG_STATE_QUERY_1, out, written);
            break;
    }
}
void SerialProcessor::ProcessSettingQuery1(const byte *in, size_t len, byte *out, size_t *written)
{
    if (len != 1)
    {
        WriteCommonErrorResponse(BOCP_E_INVALID_REQUEST, BOCP_MSG_SETTING_QUERY_1, out, written);
        return;
    }

    byte settingCode;
    uint32_t setting;
    BocpStatusCode statusCode;

    uint8_t settingCount = InternalStorage::GetSettingCount();
    out[0] = BOCP_MSG_SETTING_QUERY_2;
    out[1] = settingCount;

    for (uint8_t i = 0; i < settingCount; i++)
    {
        statusCode = InternalStorage::ReadSetting(i, &setting);
        if (BOCP_FAILED(statusCode))
        {
            WriteCommonErrorResponse(statusCode, BOCP_MSG_SETTING_QUERY_1, out, written);
            return;
        }

        WriteBigEndian32((out + 2) + (i * 4), setting);
    }

    *written = 2 + (settingCount * 4);
}
void SerialProcessor::ProcessSettingUpdate1(const byte *in, size_t len, byte *out, size_t *written)
{
    if (len != 6)
    {
        WriteCommonErrorResponse(BOCP_E_INVALID_REQUEST, BOCP_MSG_SETTING_UPDATE_1, out, written);
        return;
    }

    byte settingCode;
    int32_t newValue;
    BocpStatusCode statusCode;

    settingCode = in[1];
    newValue = ReadBigEndian32(in + 2);
    statusCode = InternalStorage::UpdateSetting(settingCode, newValue);

    if (BOCP_FAILED(statusCode))
    {
        WriteCommonErrorResponse(statusCode, BOCP_MSG_SETTING_UPDATE_1, out, written);
        return;
    }

    out[0] = BOCP_MSG_SETTING_UPDATE_2;
    out[1] = settingCode;
    *written = 2;
}
void SerialProcessor::ProcessFineControl1(const byte *in, size_t len, byte *out, size_t *written)
{
    if (len != 5)
    {
        WriteCommonErrorResponse(BOCP_E_INVALID_REQUEST, BOCP_MSG_FINE_CONTROL_1, out, written);
        return;
    }

    int32_t steps;
    BocpStatusCode statusCode;

    steps = ReadBigEndian32(in + 1);

    OscillationController *pOscCtl = g_scheduler.GetOscillationController();
    statusCode = pOscCtl->FineControl(steps);

    if (BOCP_FAILED(statusCode))
    {
        WriteCommonErrorResponse(statusCode, BOCP_MSG_FINE_CONTROL_1, out, written);
        return;
    }

    out[0] = BOCP_MSG_FINE_CONTROL_2;
    WriteBigEndian32(out + 1, statusCode);
    *written = 3;
}
void SerialProcessor::ProcessConstantVelocityMove1(const byte *in, size_t len, byte *out, size_t *written)
{
    if (len != 5)
    {
        WriteCommonErrorResponse(BOCP_E_INVALID_REQUEST, BOCP_MSG_CONSTANT_VELOCITY_MOVE_1, out, written);
        return;
    }

    BocpStatusCode statusCode;
    uint32_t encodedFrequency = ReadBigEndian32(in + 1);
    if (BOCP_FREQ_ABS_INT_PART(encodedFrequency) > 2000000)
    {
        statusCode = BOCP_E_OUT_OF_RANGE;
    }
    else
    {
        OscillationController *pOscCtl = g_scheduler.GetOscillationController();
        statusCode = pOscCtl->StartConstantVelocityMode(encodedFrequency);
    }

    out[0] = BOCP_MSG_CONSTANT_VELOCITY_MOVE_2;
    WriteBigEndian16(out + 1, (uint16_t)statusCode);
    *written = 3;
}
void SerialProcessor::ProcessStopOscillation1(const byte *in, size_t len, byte *out, size_t *written)
{
    if (len != 1)
    {
        WriteCommonErrorResponse(BOCP_E_INVALID_REQUEST, BOCP_MSG_STOP_OSCILLATION_1, out, written);
        return;
    }

    OscillationController *pOscCtl = g_scheduler.GetOscillationController();
    pOscCtl->Stop();

    out[0] = BOCP_MSG_STOP_OSCILLATION_2;
    WriteBigEndian16(out + 1, BOCP_E_SUCCESS);
    *written = 3;
}
void SerialProcessor::ProcessPredefinedWaveformOscillationMode1(const byte *in, size_t len, byte *out, size_t *written)
{
    if (len < 2)
    {
        WriteCommonErrorResponse(BOCP_E_INVALID_REQUEST, BOCP_MSG_PREDEFINED_WAVEFORM_OSCILLATION_MODE_1, out, written);
        return;
    }

    byte waveformType = in[1];
    OscillationController *pOscCtl = g_scheduler.GetOscillationController();
    BocpStatusCode status;
    
    switch (waveformType)
    {
    case WAVEFORM_SINUSOIDAL:
        if (len != 14)
        {
            WriteCommonErrorResponse(BOCP_E_INVALID_REQUEST, BOCP_MSG_PREDEFINED_WAVEFORM_OSCILLATION_MODE_1, out, written);
            return;
        }
        {
            float amplitude = ReadSinglePrecision(in + 2);
            float angularFrequency = ReadSinglePrecision(in + 6);
            float phaseConstant = ReadSinglePrecision(in + 10);

            //amplitude = 200.0;
            //angularFrequency = 2 * PI;
            //phaseConstant = 0.0;
    
            status = pOscCtl->StartSinusoidalDrive(amplitude, angularFrequency, phaseConstant);
        }
        break;
    default:
        status = BOCP_E_UNSUPPORTED;
        break;
    }

    out[0] = BOCP_MSG_PREDEFINED_WAVEFORM_OSCILLATION_MODE_2;
    WriteBigEndian16(out + 1, status);
    *written = 3;
}
void SerialProcessor::ProcessHdmPrepare1(const byte *in, size_t len, byte *out, size_t *written)
{
    if (len != 1)
    {
        WriteCommonErrorResponse(BOCP_E_INVALID_REQUEST, BOCP_MSG_HDM_PREPARE_1, out, written);
        return;
    }

    OscillationController *pOscCtl = g_scheduler.GetOscillationController();
    BocpStatusCode statusCode = pOscCtl->HdmPrepare();
    if (BOCP_FAILED(statusCode))
    {
        WriteCommonErrorResponse(statusCode, BOCP_MSG_HDM_PREPARE_1, out, written);
        return;
    }

    out[0] = BOCP_MSG_HDM_PREPARE_2;
    *written = 1;
}
void SerialProcessor::ProcessHdmInitialBufferingReq(const byte *in, size_t len, byte *out, size_t *written)
{
    // HDM 관련 통신은 여기서 수행한다.
    // 내부에 HDM 작동 모듈을 따로 두고, 거기로 메시지들을 포워딩하자.
}
void SerialProcessor::ProcessHdmOscData(const byte *in, size_t len, byte *out, size_t *written)
{
}
void SerialProcessor::ProcessHdmStop1(const byte *in, size_t len, byte *out, size_t *written)
{
}

void SerialProcessor::ProcessControlMotorPower1(const byte *in, size_t len, byte *out, size_t *written)
{
    if (len != 2)
    {
        WriteCommonErrorResponse(BOCP_E_INVALID_REQUEST, BOCP_MSG_CONTROL_MOTOR_POWER_1, out, written);
        return;
    }

    OscillationController *pOscCtl = g_scheduler.GetOscillationController();
    bool fPowerOn = !!in[1]; // sanitize to boolean value

    (fPowerOn) ? pOscCtl->PowerOnMotor() : pOscCtl->PowerOffMotor();

    out[0] = BOCP_MSG_CONTROL_MOTOR_POWER_2;
    WriteBigEndian16(out + 1, BOCP_E_SUCCESS);
    *written = 3;
}

