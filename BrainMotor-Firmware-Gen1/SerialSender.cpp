
#include <Arduino.h>
#include "SerialSender.h"

#include "BocpDefinitions.h"
#include "Crc16_CCITT.h"

#define SS_STATE_READY 0
#define SS_STATE_SENDING_SYNCSIG 1
#define SS_STATE_SENDING_BODY 2
#define SS_STATE_SEND_COMPLETE 3

SerialSender::SerialSender()
{
    m_state = SS_STATE_READY;
}
SerialSender::~SerialSender()
{
}

void SerialSender::RunQuantum()
{
    switch (m_state)
    {
        case SS_STATE_SENDING_SYNCSIG:
            SendSyncSigQuantum();
            break;
        case SS_STATE_SENDING_BODY:
            SendBodyQuantum();
            break;
    }
}
void SerialSender::SendSyncSigQuantum()
{
    int remainingZeros = BOCP_SYNC_ZERO_BYTES - m_syncSignalSentBytes;
    int sendingBytes = min(Serial.availableForWrite(), remainingZeros);
    for (int i = 0; i < sendingBytes; i++)
    {
        Serial.write(0x00);
    }
    m_syncSignalSentBytes += sendingBytes;

    if (m_syncSignalSentBytes >= BOCP_SYNC_ZERO_BYTES)
    {
        m_state = SS_STATE_SENDING_BODY;
    }
}
void SerialSender::SendBodyQuantum()
{
    if (Serial.availableForWrite())
    {
        Serial.write(m_cobsEncoder.GetNextByte());
        if (m_cobsEncoder.IsEncodingComplete())
        {
            Serial.write(0x00); // 1바이트의 blocking write는 그냥 넘어간다. 나중에 수정하려면 수정한다.
            m_cobsEncoder.Reset();

            m_state = SS_STATE_SEND_COMPLETE;
        }
    }
}
bool SerialSender::StartSending(const void *buf, int len, byte flags, byte seqNum, bool sendSyncSignal)
{
    if (m_state != SS_STATE_READY)
        return false;
    if (buf == NULL)
        return false;
    if (len < 0 || len > BOCP_MAX_PAYLOAD_SIZE)
        return false;

    GenerateSavePacket(buf, len, flags, seqNum);
    m_cobsEncoder.SetInputBuffer(m_packet, m_packetLength);
    m_bSendSyncSignal = sendSyncSignal;

    if (m_bSendSyncSignal)
    {
        m_syncSignalSentBytes = 0;
        m_state = SS_STATE_SENDING_SYNCSIG;
    }
    else
    {
        m_state = SS_STATE_SENDING_BODY;
    }

    return true;
}
void SerialSender::GenerateSavePacket(const void *buf, int len, byte flags, byte seqNum)
{
    m_packet[BOCP_PAYLOADLENGTH_FIELD_OFFSET] = (byte)len;
    m_packet[BOCP_FLAGS_FIELD_OFFSET] = flags;
    m_packet[BOCP_SEQNUM_FIELD_OFFSET] = seqNum;
    memcpy(m_packet + BOCP_HEADER_SIZE, buf, len);

    uint16_t crc16 = Crc16_CCITT(m_packet, len + BOCP_HEADER_SIZE);
    m_packet[len + BOCP_HEADER_SIZE + 0] = (byte)(crc16 >> 8);
    m_packet[len + BOCP_HEADER_SIZE + 1] = (byte)(crc16);
    m_packet[len + BOCP_HEADER_SIZE + 2] = 0x00;
    m_packetLength = len + BOCP_FRAME_SIZE;
}
bool SerialSender::IsSendComplete()
{
    return (m_state == SS_STATE_SEND_COMPLETE);
}
void SerialSender::Reset()
{
    m_state = SS_STATE_READY;
}

