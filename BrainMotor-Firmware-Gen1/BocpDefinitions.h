
#pragma once

#define BOCP_BAUD_RATE 115200UL
#define BOCP_COBS_DELIMITER 0x00

#define BOCP_HEADER_SIZE 3
#define BOCP_TRAILER_SIZE 3
#define BOCP_FRAME_SIZE (BOCP_HEADER_SIZE + BOCP_TRAILER_SIZE) // 하나의 패킷에서 BOCP 프레임만의 크기. (헤더 + 트레일러)
#define BOCP_MAX_PAYLOAD_SIZE 255
#define BOCP_MIN_PACKET_SIZE (BOCP_FRAME_SIZE)
#define BOCP_MAX_PACKET_SIZE (BOCP_FRAME_SIZE + BOCP_MAX_PAYLOAD_SIZE)

// COBS 알고리즘으로 BOCP_MAX_PACKET_SIZE(=261)개의 바이트를 byte stuffing하여 나올 수 있는 최악의 경우는,
// 254개의 non-zero byte가 연속될 때 발생하는 오버헤드 1바이트뿐이다.
// 메시지 끝에는 항상 강제로 00을 붙이기 때문에, 이로 인한 오버헤드는 이미 패킷의 크기에 반영되어 있다.
// 따라서 패킷의 최대 크기는 BOCP 최대 패킷 크기에 1을 더한 것이 된다.
#define BOCP_MAX_STUFFED_PACKET_SIZE (BOCP_MAX_PACKET_SIZE + 1)

#define BOCP_SYNC_ZERO_BYTES 16

#define BOCP_CONNECTION_TIMEOUT_MILLISECONDS (500)
#define BOCP_CONNECTION_TIMEOUT_MICROSECONDS (BOCP_CONNECTION_TIMEOUT_MILLISECONDS * 1000)

#define BOCP_PAYLOADLENGTH_FIELD_OFFSET 0
#define BOCP_FLAGS_FIELD_OFFSET 1
#define BOCP_SEQNUM_FIELD_OFFSET 2

#define BOCP_NEXT_SEQNUM(x) ((byte)(x) == 0xFF ? (byte)0x00 : ((byte)(x) + 1))

// BOCP 주파수 인코딩 및 디코딩
#define BOCP_FREQ_ABS_INT_PART(f) (((uint32_t)(f) & 0x7fffffff) >> 8)
#define BOCP_FREQ_ABS_FRAC_PART(f) ((uint32_t)(f) & 0xff)

// BOCP 패킷 프레임 플래그
#define BOCP_FLAGS_NONE 0x00
#define BOCP_FLAGS_SYN 0x01
#define BOCP_FLAGS_FIN 0x02
#define BOCP_FLAGS_RTR 0x04

// BOCP 상태 코드 (StatusCode)
typedef int16_t BocpStatusCode;
#define BOCP_SUCCEEDED(sc) (((BocpStatusCode)(sc)) >= 0)
#define BOCP_FAILED(sc) (!BOCP_SUCCEEDED(sc))
#define BOCP_E_SUCCESS 0x0000
#define BOCP_E_UNKNOWN 0xFFFF
#define BOCP_E_INVALID_REQUEST 0x8000
#define BOCP_E_UNSUPPORTED 0x8001
#define BOCP_E_OUT_OF_RANGE 0x8002
#define BOCP_E_INVALID_PARAMETER 0x8003
#define BOCP_E_INVALID_OPERATION 0x8004
#define BOCP_E_TIMEOUT 0x8005
#define BOCP_E_PROTOCOL 0x8006
#define BOCP_E_SERIAL_IO 0x8007
#define BOCP_E_CANCELED 0x8008

// BOCP 메시지 종류 (MsgType)
#define BOCP_MSG_COMMON_ERROR_RESPONSE 0x00
#define BOCP_MSG_ECHO_1 0x01
#define BOCP_MSG_ECHO_2 0x02
#define BOCP_MSG_TIME_SYNC_1 0x03
#define BOCP_MSG_TIME_SYNC_2 0x04
#define BOCP_MSG_SPEC_QUERY_1 0x20
#define BOCP_MSG_SPEC_QUERY_2 0x21
#define BOCP_MSG_STATE_QUERY_1 0x22
#define BOCP_MSG_STATE_QUERY_2 0x23
#define BOCP_MSG_SETTING_QUERY_1 0x24
#define BOCP_MSG_SETTING_QUERY_2 0x25
#define BOCP_MSG_SETTING_UPDATE_1 0x26
#define BOCP_MSG_SETTING_UPDATE_2 0x27
#define BOCP_MSG_FINE_CONTROL_1 0x36
#define BOCP_MSG_FINE_CONTROL_2 0x37
#define BOCP_MSG_CONSTANT_VELOCITY_MOVE_1 0x28
#define BOCP_MSG_CONSTANT_VELOCITY_MOVE_2 0x29
#define BOCP_MSG_STOP_OSCILLATION_1 0x2A
#define BOCP_MSG_STOP_OSCILLATION_2 0x2B
#define BOCP_MSG_PREDEFINED_WAVEFORM_OSCILLATION_MODE_1 0x2C
#define BOCP_MSG_PREDEFINED_WAVEFORM_OSCILLATION_MODE_2 0x2D
#define BOCP_MSG_HDM_PREPARE_1 0x2E
#define BOCP_MSG_HDM_PREPARE_2 0x2F
#define BOCP_MSG_HDM_INITIAL_BUFFERING_REQ 0x30
#define BOCP_MSG_HDM_INITIAL_BUFFERING_RSP 0x31
#define BOCP_MSG_HDM_OSC_DATA 0x32
#define BOCP_MSG_HDM_OSC_DATA_RSP 0x33
#define BOCP_MSG_HDM_STOP_1 0x34
#define BOCP_MSG_HDM_STOP_2 0x35
#define BOCP_MSG_CONTROL_MOTOR_POWER_1 0x38
#define BOCP_MSG_CONTROL_MOTOR_POWER_2 0x39
#define BOCP_MSGTYPE_INVALID 0xFF

// 진동 상태
typedef uint8_t OscillationMode;
#define OSC_MODE_STOPPED 0x00
#define OSC_MODE_CONSTANT_VELOCITY 0x01
#define OSC_MODE_HDM 0x02
#define OSC_MODE_CALIBRATION 0x03
#define OSC_MODE_PREDEFINED_WAVEFORM 0x04

// HDM 상태
#define HDM_STATE_NONE 0
#define HDM_STATE_INITIAL_BUFFERING 1
#define HDM_STATE_OSC_DATA_TRANSMISSION 2

// 모터 전원 상태
#define MOTOR_POWER_ON 0x01
#define MOTOR_POWER_OFF 0x00

// 설정 코드
#define BOCP_SETTING_COUNT_MAX 0x3F
#define BOCP_SETTING_CODE_MAX (BOCP_SETTING_COUNT_MAX - 1)

#define SETTING_CODE_OPERATION_MODE 0x00
#define SETTING_CODE_PULSE_PER_REV 0x01
#define SETTING_CODE_DISTANCE_PER_REV 0x02
#define SETTING_CODE_MAX SETTING_CODE_DISTANCE_PER_REV

// 운동 방향
#define DIRECTION_A 0x00
#define DIRECTION_B 0x01

// 운동 종류
#define ROTATIONAL_MOTION 0x00
#define LINEAR_MOTION 0x01

// 모터 홀딩
#define OPERATION_MODE_HOLD_MOTOR 0x04

// 미리 정의된 파형
#define WAVEFORM_SINUSOIDAL 0x00

