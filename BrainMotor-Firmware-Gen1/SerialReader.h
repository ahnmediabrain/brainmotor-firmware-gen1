
#pragma once

#include <stdint.h>

#include "BocpDefinitions.h"
#include "TaskBase.h"

class Scheduler;

class SerialReader : public TaskBase
{
public:
    SerialReader();
    virtual ~SerialReader();

public:
    virtual void RunQuantum();

private:
    void ProcessDelimiter();
    // COBS 디코더가 새로운 입력을 받을 수 있도록 초기화
    void ResetPrepareCOBSDecoder();
    // COBS 디코더에 바이트 1개를 입력함.
    // 만약 BOCP 패킷의 최대 크기를 초과했거나, 잘못된 COBS 시퀀스라면, false를 반환.
    // @param incomingByte 입력할 바이트. 0x00이 아닌 바이트여야 함.
    bool FeedCOBSDecoder(byte incomingByte);
    // 현재까지 입력된 데이터가 COBS 시퀀스의 경계이면
    // true 반환. 중간에 잘리는 경우라면 false를 반환.
    bool IsCOBSSequenceFinished();
    // 수신한 데이터가 올바른지 검사
    bool CheckMessageIsValid();

public:
    // 메시지가 처리 가능하게 준비되었는지 쿼리
    bool IsPacketReady();
    // 수신기의 상태를 초기상태로 되돌림
    void Reset();

    /// 이하의 PacketGet으로 시작하는 함수들은 수신된 패킷에 담겨있는 정보를 쿼리하는 함수들이다.

    // 메시지가 처리 가능하게 준비된 경우 버퍼의 시작주소 반환
    // (준비되지 않은 경우 NULL 반환)
    const byte *PacketGetPayloadBuffer();
    // 메시지가 처리 가능하게 준비된 경우 메시지의 payload의 길이 반환
    size_t PacketGetPayloadLength();
    // 메시지가 처리 가능하게 준비된 경우 플래그 반환
    byte PacketGetFlags();
    // 메시지가 처리 가능하게 준비된 경우 시퀀스 넘버 반환
    byte PacketGetSeqNum();

private:
    Scheduler *m_pScheduler;
    uint8_t m_state;

    byte m_buf[BOCP_MAX_STUFFED_PACKET_SIZE];
    int m_unstuffedBytes;
    int m_remainingBytesUntilZero;
};

