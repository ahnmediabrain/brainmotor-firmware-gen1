
#include <Arduino.h>
#include "Crc16_CCITT.h"

#include "Scheduler.h"
#include "SerialReader.h"

class SerialReaderState
{
public:
    // 초기 상태로 Delimiter가 아직 도착하지 않았거나,
    // 해독 불가능한 잘못된 COBS 시퀀스가 도달하여
    // Delimiter가 나올 때까지 메시지를 무시해야 하는 상태
    static const uint8_t WaitingDelimiter = 0;
    // Delimiter를 수신하였고, 다음 바이트가 Delimiter가 아니면 메시지가 시작되는 상태
    static const uint8_t GotDelimiter = 1;
    // COBS 시퀀스를 수신하고 있는 중
    static const uint8_t ReceivingCOBSSequence = 2;
    // 메시지 처리를 대기하는 중
    static const uint8_t WaitingConsumption = 3;
};

SerialReader::SerialReader()
{
    Reset();
}
SerialReader::~SerialReader()
{
}

void SerialReader::RunQuantum()
{
    //if (Serial.available() > 0)
    {
        // 사실, available 데이터가 있는데 읽기 작업이 실패했다는 것은 말이 안 된다.
        // (왜냐하면, 이 프로젝트에서 시리얼 통신을 하는 모듈은 이 클래스뿐이다)
        int incomingByte = Serial.read();
        if (incomingByte == -1) return;


        if (incomingByte == BOCP_COBS_DELIMITER)
        {
            //Serial.write("Got DELIM\n");
            //Serial.write("D");
            ProcessDelimiter();
        }
        else /* 현재 수신한 바이트가 Delimiter가 아닌 경우 */
        {
        //Serial.write("Got byte ");
        //Serial.print(incomingByte, HEX);
        //Serial.write("\n");
            //Serial.write("Got DATA\n");
            switch (m_state)
            {
                case SerialReaderState::WaitingDelimiter:
                    // Delimiter가 나올 때까지 기다려야 함
                    //Serial.print("WaitDelim\n");
                    break;
                case SerialReaderState::GotDelimiter:
                    // 이전 바이트는 Delimiter이었는데 현재 수신한 바이트는 Delimiter가 아님
                    // -> COBS 디코딩 시작
                    ResetPrepareCOBSDecoder();
                    m_state = SerialReaderState::ReceivingCOBSSequence;
                    if (!FeedCOBSDecoder((byte)incomingByte))
                    {
                        m_state = SerialReaderState::WaitingDelimiter;
                        //Serial.print("Error-Reset\n");
                    }
                    break;
                case SerialReaderState::ReceivingCOBSSequence:
                    if (!FeedCOBSDecoder((byte)incomingByte))
                    {
                        m_state = SerialReaderState::WaitingDelimiter;
                        //Serial.print("Error-Reset\n");
                    }
                    break;
            }
        }
    }
}

void SerialReader::ProcessDelimiter()
{
    switch (m_state)
    {
        case SerialReaderState::WaitingDelimiter:
            m_state = SerialReaderState::GotDelimiter;
            break;
        case SerialReaderState::ReceivingCOBSSequence:
            {
                if (IsCOBSSequenceFinished() && CheckMessageIsValid())
                {
                    // 올바른 메시지
                    m_state = SerialReaderState::WaitingConsumption;
                    //Serial.write("ValidMsg\n");
                }
                else
                {
                    // 올바르지 않은 메시지
                    m_state = SerialReaderState::GotDelimiter;
                    //Serial.write("InvalidMsg\n");
                }
            }
            break;
    }
}
void SerialReader::ResetPrepareCOBSDecoder()
{
    m_unstuffedBytes = 0;
    m_remainingBytesUntilZero = 0;
}
bool SerialReader::FeedCOBSDecoder(byte incomingByte)
{
    if (incomingByte == 0x00)
        return false;

    if (m_unstuffedBytes >= BOCP_MAX_STUFFED_PACKET_SIZE)
        return false;
    
    if (m_remainingBytesUntilZero > 0)
    {
        // 데이터 바이트
        m_buf[m_unstuffedBytes++] = incomingByte;
        m_remainingBytesUntilZero--;
    }
    else
    {
        // 리딩(Leading) 바이트
        m_remainingBytesUntilZero = incomingByte - 1;
    }

    // 현재 subsequence에서 남은 바이트 수가 0이면(다 받았으면), Delimiter를 기록
    if (m_remainingBytesUntilZero == 0)
    {
        if (m_unstuffedBytes >= BOCP_MAX_STUFFED_PACKET_SIZE)
            return false;
        m_buf[m_unstuffedBytes++] = BOCP_COBS_DELIMITER;
    }
    return true;
}
bool SerialReader::IsCOBSSequenceFinished()
{
    return (m_remainingBytesUntilZero == 0);
}
bool SerialReader::CheckMessageIsValid()
{
    // 현재 메시지의 길이 확인
    int msgLength = m_buf[BOCP_PAYLOADLENGTH_FIELD_OFFSET];
    if (m_unstuffedBytes < BOCP_MIN_PACKET_SIZE || m_unstuffedBytes > BOCP_MAX_PACKET_SIZE)
        return false;
    if (msgLength + BOCP_FRAME_SIZE != m_unstuffedBytes)
        return false;

    // 메시지의 끝이 '00'으로 끝나는 지 확인
    if (m_buf[m_unstuffedBytes - 1] != 0x00)
        return false;

    // CRC 확인 (* FIXME: incremental하게 하도록 바꿀 것!)
    uint16_t crc16_calculated = Crc16_CCITT(m_buf, m_unstuffedBytes - BOCP_TRAILER_SIZE);
    uint16_t crc16_received = (((uint16_t)m_buf[m_unstuffedBytes - BOCP_TRAILER_SIZE]) << 8) | (m_buf[m_unstuffedBytes - BOCP_TRAILER_SIZE + 1]); // Big Endian
    if (crc16_calculated != crc16_received)
        return false;

    return true;
}
bool SerialReader::IsPacketReady()
{
    return (m_state == SerialReaderState::WaitingConsumption);
}
void SerialReader::Reset()
{
    ResetPrepareCOBSDecoder();
    m_state = SerialReaderState::WaitingDelimiter;
}

const byte *SerialReader::PacketGetPayloadBuffer()
{
    if (!IsPacketReady())
        return NULL;

    return m_buf + BOCP_HEADER_SIZE;
}
size_t SerialReader::PacketGetPayloadLength()
{
    if (!IsPacketReady())
        return -1;

    return (size_t)(m_buf[BOCP_PAYLOADLENGTH_FIELD_OFFSET]);
}
byte SerialReader::PacketGetFlags()
{
    if (!IsPacketReady())
        return (byte)-1;

    return m_buf[BOCP_FLAGS_FIELD_OFFSET];
}
byte SerialReader::PacketGetSeqNum()
{
    if (!IsPacketReady())
        return (byte)-1;

    return m_buf[BOCP_SEQNUM_FIELD_OFFSET];
}

