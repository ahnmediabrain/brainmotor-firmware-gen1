    
#pragma once

#include "OscillationController.h"
#include "SerialModule.h"

class Scheduler
{
public:
    Scheduler();
    ~Scheduler();

public:
    void Initialize();
    void Schedule();

public:
    OscillationController *GetOscillationController();

private:
    OscillationController *m_pOscillationModule;
    SerialModule *m_pSerialModule;
};

