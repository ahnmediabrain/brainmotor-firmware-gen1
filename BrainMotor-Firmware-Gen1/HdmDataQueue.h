﻿
#pragma once

#include <stdint.h>

#include "BocpDefinitions.h"
#include "Time.h"
#include "DeviceSpec.h"

typedef struct
{
    MICROSECOND64 timestampBase; // 이 항목은 HDM 도중에 변하지 않는다.
    uint32_t headIndexUpper;
    uint32_t headIndexLower;
    uint32_t *head;
    uint32_t *tail;
    bool isFull;
    uint32_t buffer[HDM_BUFFER_CAPACITY];

} HDM_DATA_QUEUE;

void HdmInitQueue(HDM_DATA_QUEUE *pHdmDataQueue);
void HdmSetTimestampBase(MICROSECOND64 timestampBase);
//void HdmSupplySamples(uint32_t firstSampleIndex, 

int HdmGetLastSampleIndex(const HDM_DATA_QUEUE *pHdmDataQueue);
int HdmGetDataCount(const HDM_DATA_QUEUE *pHdmDataQueue); // 단순히 큐에 저장된 데이터 수
int HdmGetSamplesAhead(const HDM_DATA_QUEUE *pHdmDataQueue); // 현재 시간으로부터 몇 ms 버틸 수 있는지 샘플 개수
BocpStatusCode HdmTryGetOscillationData(const HDM_DATA_QUEUE *pHdmDataQueue, MICROSECOND64 currentTime, uint32_t *pEncodedFrequency);
