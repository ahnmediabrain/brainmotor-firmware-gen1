
#pragma once

#include "TaskBase.h"
#include "BocpDefinitions.h"
#include "HdmDataQueue.h"

class OscillationController : public TaskBase
{
public:
    OscillationController();
    virtual ~OscillationController();

public:
    virtual void RunQuantum();

public:
    bool IsMotorPoweredOn();
    BocpStatusCode PowerOnMotor();
    BocpStatusCode PowerOffMotor();
private:
    void PowerOnMotorInternal();
    void PowerOffMotorInternal();
    void UpdateMotorPowerState();

public:
    // Waveform의 속도가 (+)값일 때 모터를 어느 방향으로 회전시킬지를 지정한다.
    // 이것은 모터가 어떻게 설치되어 있는지에 따라 (+)방향이 의미해야 할 회전방향이 달라질 수 있기 때문에 필요하다.
    BocpStatusCode SetPlusDirection(byte direction);
    byte GetPlusDirection();

    OscillationMode GetCurrentOscillationMode();
    BocpStatusCode FineControl(int32_t steps);
    BocpStatusCode StartConstantVelocityMode(uint32_t encodedFrequency);
    BocpStatusCode StartSinusoidalDrive(float amplitude, float angularFrequency, float phaseConstant);
    BocpStatusCode StartCalibration();
    BocpStatusCode Stop();

    BocpStatusCode HdmPrepare();
    BocpStatusCode HdmSupplyInitialBufferingData();
    BocpStatusCode HdmSupplyData();
    BocpStatusCode HdmStop();

private:
    void SetMotorVelocity(uint32_t encodedFrequency); // BOCP에서 사용하는 주파수 인코딩 방식을 따르는 값을 전달해야 함
    void SetMotorFrequency(uint32_t frequencyAbsValue);
    void SetMotorDirection(byte direction);

private:
    OscillationMode m_currentOscillationMode;

    //****** !BEGIN Predefined waveform oscillation mode 관련 ********//
    // * 1024us마다 갱신함
    byte m_pwomWaveformType; // 만약 현재 진동 모드가 "미리 정의된 파형 진동 모드"라면 그 파형의 종류를 나타냄
    float m_pwomSineAmplitude;
    float m_pwomSineAngularFrequency;
    float m_pwomSinePhaseConstant;
    uint32_t m_pwomTimeBase; // 진동 시작 기준 시간
    uint32_t m_pwomLastUpdatedTime; // 마지막으로 갱신한 시간
    uint32_t m_pwomNextFreq; // 다음 순간에 사용할 주파수
    uint32_t m_pwomTimeOffset; // 진동을 시작한 순간의 micros() timestamp의 하위 10비트 (이 시점을 기준으로 진동을 진행함)

    uint32_t CalcSinusoidalDriveFreq(float amplitude, float angularFrequency, float phaseConstant, uint32_t time); // 모두 10^-6 scale
    //****** !END Predefined waveform oscillation mode 관련 ********//

    bool m_bMotorOn;
    byte m_plusDirection;
    byte m_minusDirection;

    uint32_t m_cvmEncodedFrequency;

    byte m_hdmState;
    HDM_DATA_QUEUE m_hdmDataQueue;

    // 마지막으로 확인했을 때 buffer underrun이 일어났는지를 나타냄.
    // buffer underrun의 상황에서는 버퍼에 데이터가 조금이라도 들어왔는 지
    // 계속 확인해서 현재 시간에 해당하는 값을 가져와야 하지만, buffer underrun이
    // 아니라면 직전에 설정한 시간으로부터 1024us를 대기해야 함
    bool m_hdmIsUnderrun;
    unsigned long m_hdmLastUpdatedTimeMicros; // 상위 22비트: micros()의 timestamp, 하위 10비트: m_hdmTimeOffset (-> micros()의 결과와 산술비교 가능)
    unsigned long m_hdmTimeOffset; // HDM을 시작한 순간의 micros() timestamp의 하위 10비트 (HDM에서 이 시점을 기준으로 진동을 진행함)
    uint32_t m_hdmBufferUnderruns; // count underruns for logging and debugging purposes
};

