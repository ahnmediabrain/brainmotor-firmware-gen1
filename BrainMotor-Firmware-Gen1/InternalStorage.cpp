
#include <Arduino.h>
#include "BrainMotor-Firmware-Gen1.h"
#include "InternalStorage.h"
    
void InternalStorage::ReadSerialNumber(byte *buf)
{
    memcpy(buf, "Z9RTU35F3ADKFVFK", 16);
}
BocpStatusCode InternalStorage::ReadSetting(byte settingCode, uint32_t *pOut)
{
    if (settingCode > SETTING_CODE_MAX)
        return BOCP_E_UNSUPPORTED;

    switch (settingCode)
    {
    case SETTING_CODE_OPERATION_MODE:
        *pOut = 0x00000000;//0x00000002;
        break;
    case SETTING_CODE_PULSE_PER_REV:
        *pOut = 1600;//40000;//;//1600;//400;
        break;
    case SETTING_CODE_DISTANCE_PER_REV:
        *pOut = 8000;//8000;//0;
        break;
    }
    // FIXME: 구현 추가할 것
    return BOCP_E_SUCCESS;
}
BocpStatusCode InternalStorage::UpdateSetting(byte settingCode, uint32_t newValue)
{
    // FIXME: 구현 추가할 것 (NOTE: 새로운 값은 유효성을 검사해야 함)
    return BOCP_E_UNSUPPORTED;
}

uint8_t InternalStorage::GetSettingCount()
{
    return 3;
}

/*

BocpStatusCode SerialProcessor::ProcessSettingUpdate1_PlusDirection(const byte *in, size_t len)
{
    if (len != 3)
        return BOCP_E_INVALID_REQUEST;

    byte direction = in[1];
    switch (direction)
    {
        case DIRECTION_A:
        case DIRECTION_B:
            return BOCP_E_SUCCESS;
        default:
            return BOCP_E_INVALID_PARAMETER;
    }
}

*/
