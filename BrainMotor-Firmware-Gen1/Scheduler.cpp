
#include <Arduino.h>

#include "Time.h"

#include "Scheduler.h"

Scheduler::Scheduler()
{
}

Scheduler::~Scheduler()
{
    delete m_pOscillationModule;
    delete m_pSerialModule;
}
void Scheduler::Initialize()
{
    m_pOscillationModule = new OscillationController();
    m_pSerialModule = new SerialModule(this);

    //Serial.begin(115200);
}
void Scheduler::Schedule()
{
    //Serial.write("Run Quantum");
    m_pOscillationModule->RunQuantum();
    m_pSerialModule->RunQuantum();
    Time::UpdateRolloverCountQuantum();
}

OscillationController *Scheduler::GetOscillationController()
{
    return m_pOscillationModule;
}

