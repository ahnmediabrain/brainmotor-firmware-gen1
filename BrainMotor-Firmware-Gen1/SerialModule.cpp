
#include <Arduino.h>

#include "Scheduler.h"
#include "SerialReader.h"
#include "SerialProcessor.h"
#include "SerialSender.h"

#include "SerialModule.h"

#include "BocpDefinitions.h"
#include "Time.h"

#include "LedModule.h"

#define SM_STATE_RECEIVING 0
#define SM_STATE_PROCESSING_PACKET 1
#define SM_STATE_PROCESSING_PAYLOAD 2 // 패킷의 payload 자체를 처리하는 중을 나타냄(즉 SYN/FIN/RTR 등이 아닌 일반 패킷이라는 의미)
#define SM_STATE_SENDING 3

SerialModule::SerialModule(Scheduler *pScheduler)
{
    m_pScheduler = pScheduler;
    m_state = SM_STATE_RECEIVING;
    m_connectedToHost = false;

    Serial.begin(BOCP_BAUD_RATE);//, SERIAL_8E1);
}
SerialModule::~SerialModule()
{
    Serial.end();
}

void SerialModule::RunQuantum()
{
    //Serial.write("Run Quantum");
    switch (m_state)
    {
        case SM_STATE_RECEIVING:
            m_reader.RunQuantum();
            if (m_reader.IsPacketReady())
            {
                //Serial.write("Pac");
                m_state = SM_STATE_PROCESSING_PACKET;
            }
            break;
        case SM_STATE_PROCESSING_PACKET:
            //if (m_reader.PacketGetFlags() & BOCP_FLAGS_SYN == 0x00)
            //{
                //Serial.write("Proc");
                //Serial.write("ProcSwi");
            //}
            ProcessPacket();
            break;
        case SM_STATE_PROCESSING_PAYLOAD:
            //Serial.write("Q");
            m_processor.ProcessMessage(m_reader.PacketGetPayloadBuffer(), m_reader.PacketGetPayloadLength(), m_buf, &m_bufDataLength);
            m_bufDataSeqNum = m_seqNum;

            m_state = SM_STATE_SENDING;
            m_sender.StartSending(m_buf, m_bufDataLength, BOCP_FLAGS_NONE, m_bufDataSeqNum, m_reader.PacketGetFlags() & BOCP_FLAGS_RTR);
            break;
        case SM_STATE_SENDING:
            m_sender.RunQuantum();
            if (m_sender.IsSendComplete())
            {
                m_reader.Reset();
                m_sender.Reset();
                m_state = SM_STATE_RECEIVING;
            }
            break;
    }

    // 타임아웃 처리
    if (m_connectedToHost && m_state == SM_STATE_RECEIVING)
    {
        unsigned long currentTimestamp = Time::Micros();
        if (currentTimestamp - m_lastReceivedTimestamp >= BOCP_CONNECTION_TIMEOUT_MICROSECONDS)
        {
            m_processor.ConnectionReset();
            m_connectedToHost = false;
            LedModule::Off(LED_HOST_CONNECTION);
        
            m_reader.Reset();
            m_state = SM_STATE_RECEIVING;
        }
    }
}
void SerialModule::ProcessPacket()
{
    if (ProcessSYN()) return;
    if (ProcessFIN()) return;
    if (ProcessRTR()) return;
    if (ProcessNormal()) return;

    //Serial.write("ERROR p\n");

    // 패킷이 어떤 조건으로도 처리되지 못 했음; 즉, 구조 자체가
    // 올바르지 못한 패킷임. 이런 패킷에 대해서는 응답을 전송하지 않고 drop시킨다.
    // (이런 패킷에 대해서는, 가장 최근에 받은 패킷의 타임스탬프로 기록하지 않는다)
    m_reader.Reset();
    m_state = SM_STATE_RECEIVING;
}
bool SerialModule::ProcessSYN()
{
    if (!IsValidSYN())
        return false;

    byte seqNum = m_reader.PacketGetSeqNum();

    // 연결 초기화
    if (m_connectedToHost)
    {
        // 현재 연결되어 있는 상태라고 내부 변수에 설정되어 있는데
        // SYN을 받았다는 것은, 연결이 중간에 끊어졌다가 지금 다시 연결된다는 뜻임.
        // Processor가 연결 리셋에 대한 처리를 할 수 있도록
        // ConnectionReset을 호출해 줌.
        m_processor.ConnectionReset();
    }
    m_connectedToHost = true;
    LedModule::On(LED_HOST_CONNECTION);
    m_processor.ConnectionEstablished();

    m_bufDataSeqNum = m_seqNum = seqNum;
    m_bufDataLength = 0;

    m_state = SM_STATE_SENDING;
    m_sender.StartSending(m_buf, m_bufDataLength, BOCP_FLAGS_SYN, m_bufDataSeqNum, true);

    m_lastReceivedTimestamp = Time::Micros();
    return true;
}
bool SerialModule::IsValidSYN()
{
    byte flags = m_reader.PacketGetFlags();
    size_t payloadLength = m_reader.PacketGetPayloadLength();

    return ((flags & BOCP_FLAGS_SYN) == BOCP_FLAGS_SYN) &&
            ((flags & BOCP_FLAGS_FIN) == 0) &&
            ((flags & BOCP_FLAGS_RTR) == 0) &&
            (payloadLength == 0);
}
bool SerialModule::ProcessFIN()
{
    if (!IsValidFIN())
        return false;

    m_processor.ConnectionEnded();
    m_connectedToHost = false;
    LedModule::Off(LED_HOST_CONNECTION);

    m_reader.Reset();
    m_state = SM_STATE_RECEIVING;

    m_lastReceivedTimestamp = Time::Micros();
    return true;
}
bool SerialModule::IsValidFIN()
{
    byte flags = m_reader.PacketGetFlags();
    size_t payloadLength = m_reader.PacketGetPayloadLength();

    return (m_connectedToHost) &&
            ((flags & BOCP_FLAGS_SYN) == 0) &&
            ((flags & BOCP_FLAGS_FIN) == BOCP_FLAGS_FIN) &&
            ((flags & BOCP_FLAGS_RTR) == 0) &&
            (payloadLength == 0);
}
bool SerialModule::ProcessRTR()
{
    if (!IsValidRTR())
        return false;
    
    byte seqNum = m_reader.PacketGetSeqNum();
    if (seqNum == m_bufDataSeqNum)
    {
        // 패킷 재전송
        m_state = SM_STATE_SENDING;
        m_sender.StartSending(m_buf, m_bufDataLength, BOCP_FLAGS_NONE, m_bufDataSeqNum, true);

        m_lastReceivedTimestamp = Time::Micros();
        return true;
    }
    else if (seqNum == BOCP_NEXT_SEQNUM(m_seqNum))
    {
        // 첫 번째로 전송된 패킷이 드롭되고, (몇 번째인지는 모르지만) 지금 이 시점에서 그 패킷을 처음으로 받은 경우.
        // -> 이 경우는 payload를 처리해야 함

        // payload의 길이 확인은 역시 필요하다
        size_t payloadLength = m_reader.PacketGetPayloadLength();
        if (payloadLength == 0)
            return false;

        m_seqNum = seqNum;
        m_state = SM_STATE_PROCESSING_PAYLOAD;

        m_lastReceivedTimestamp = Time::Micros();
        return true;
    }
    return false;
}
bool SerialModule::IsValidRTR()
{
    byte flags = m_reader.PacketGetFlags();
    size_t payloadLength = m_reader.PacketGetPayloadLength();

    return (m_connectedToHost) &&
            ((flags & BOCP_FLAGS_SYN) == 0) &&
            ((flags & BOCP_FLAGS_FIN) == 0) &&
            ((flags & BOCP_FLAGS_RTR) == BOCP_FLAGS_RTR);
}
bool SerialModule::ProcessNormal()
{
    byte flags = m_reader.PacketGetFlags();
    byte seqNum = m_reader.PacketGetSeqNum();
    size_t payloadLength = m_reader.PacketGetPayloadLength();

    if (seqNum != BOCP_NEXT_SEQNUM(m_seqNum))
    {
        // sequence number mismatch
        return false;
    }
    m_seqNum = seqNum;

    //if (flags != BOCP_FLAGS_NONE)
    //    return false;
    //if (payloadLength == 0)
    //    return false;

    //Serial.write("Processing payload\n");
    m_state = SM_STATE_PROCESSING_PAYLOAD;

    m_lastReceivedTimestamp = Time::Micros();
    return true;
}

