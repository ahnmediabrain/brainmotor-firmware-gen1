
#include <Arduino.h>
#include "EndianUtility.h"

void WriteBigEndian16(byte *buf, uint16_t value)
{
    buf[0] = (byte)(value >> 8);
    buf[1] = (byte)(value);
}
void WriteBigEndian32(byte *buf, uint32_t value)
{
    buf[0] = (byte)(value >> 24);
    buf[1] = (byte)(value >> 16);
    buf[2] = (byte)(value >> 8);
    buf[3] = (byte)(value);
}
uint16_t ReadBigEndian16(const byte *buf)
{
    return (((uint16_t)buf[0]) << 8) |
        ((uint16_t)buf[1]);
}
uint32_t ReadBigEndian32(const byte *buf)
{
    return (((uint32_t)buf[0]) << 24) |
        (((uint32_t)buf[1]) << 16) |
        (((uint32_t)buf[2]) << 8) |
        ((uint32_t)buf[3]);
}

float ReadSinglePrecision(const byte *buf)
{
    return *(float *)(buf); // big-endian -> big-endian; no conversion necessary
}

