
#pragma once

#include <stdint.h>

void WriteBigEndian16(byte *buf, uint16_t value);
void WriteBigEndian32(byte *buf, uint32_t value);
uint16_t ReadBigEndian16(const byte *buf);
uint32_t ReadBigEndian32(const byte *buf);

float ReadSinglePrecision(const byte *buf);

