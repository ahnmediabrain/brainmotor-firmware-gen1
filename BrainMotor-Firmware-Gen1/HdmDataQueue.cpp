
#include "HdmDataQueue.h"

/*
typedef struct
{
    MICROSECOND64 timestampBase; // 이 항목은 HDM 도중에 변하지 않는다.
    uint32_t headIndexUpper;
    uint32_t headIndexLower;
    uint32_t *head;
    uint32_t *tail;
    bool isFull;
    uint32_t buffer[HDM_BUFFER_CAPACITY];

} HDM_DATA_QUEUE;
*/

int HdmGetDataCount(const HDM_DATA_QUEUE *q)
{
    if (!q)
        return -1;

    if (q->head < q->tail) return q->tail -q->head;
    else if (q->head > q->tail) return (HDM_BUFFER_CAPACITY) - (q->head - q->tail);

    return q->isFull ? HDM_BUFFER_CAPACITY : 0;
}

BocpStatusCode HdmTryGetOscillationData(const HDM_DATA_QUEUE *pHdmDataQueue, MICROSECOND64 currentTime, uint32_t *pEncodedFrequency)
{
    /* 1. 데이터의 시작과 끝을 계산
     * 2. 현재 데이터의 시간 범위의 시작과 끝 사이에 있는지 조사
     * 3. HDM_TIME_UNIT_MICROSECONDS로 나누어서 index를 구함
     * 4. 해당 Index의 데이터를 가져옴
     *   - 
     */
     return BOCP_E_SUCCESS;
}

