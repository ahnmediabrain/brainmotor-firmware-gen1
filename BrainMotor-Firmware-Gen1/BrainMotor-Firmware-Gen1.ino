
#include <Arduino.h>

#include "PWM.h"

#include "LedModule.h"
#include "Scheduler.h"

Scheduler g_scheduler;

void setup()
{
    LedModule::Initialize();
    LedModule::On(LED_ARDUINO_POWER);
    
    // PWM 라이브러리 초기화
    // 'Safe' 접두어는 Timer 관련 함수(delay, delayMicroseconds) 등에 영향을 주지 않는 한도 내에서
    // 가변주파수 PWM을 Set-up함. Mega 2560에서 11번 pin은 delay, delayMicroseconds 등의 함수에서 사용하는 TIMER와
    // 연결되어 있지 않아 11번 pin을 PWM에 사용할 수 있다.
    // NOTE: 이 함수로의 호출이 반드시 있어야 함. 그렇지 않으면 주파수의 원하는 resolution을 얻을 수 없다(너무 양자화됨)
    InitTimersSafe();

    g_scheduler.Initialize();
}

void loop()
{
    g_scheduler.Schedule();
}

