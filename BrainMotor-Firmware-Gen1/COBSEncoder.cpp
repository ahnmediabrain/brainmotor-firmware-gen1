
#include <Arduino.h>
#include "COBSEncoder.h"

#define COBSENC_STATE_READY 0
#define COBSENC_STATE_LEADING_ZERO 1
#define COBSENC_STATE_NEW_SEQUENCE 2 // COBS 시퀀스를 나타낸다.
#define COBSENC_STATE_COPYING_BYTES 3
#define COBSENC_STATE_TRAILING_ZERO 4
#define COBSENC_STATE_COMPLETE 5

COBSEncoder::COBSEncoder()
{
    Reset();
}
COBSEncoder::~COBSEncoder()
{
}

void COBSEncoder::Reset()
{
    m_state = COBSENC_STATE_READY;
}
bool COBSEncoder::SetInputBuffer(const void *input, size_t length)
{
    if (input == NULL || length == 0)
        return false;

    m_inputBase = (const uint8_t *)input;
    m_length = length;

    m_bufPos = 0;

    m_state = COBSENC_STATE_LEADING_ZERO;
    return true;
}
int COBSEncoder::GetNextByte()
{
    // 1) zero uint8_t 찾기
    // 2) 254 바이트를 봐도 안나오면, code를 0xFF로. copy할 데이터양은 254바이트.
    //    input[N]이 zero uint8_t이면, code를 (N+1)로. copy할 데이터양은 N바이트.
    // 3) code 기록
    // 4) 2에서 계산한 만큼 데이터 복사
    // 5) input <- input + N + 1 (zero uint8_t skip)
    // 6) 1로 되돌아감
    // * 아래 구현에서는 매 호출마다 한 개의 바이트씩 '기록' 또는 '복사' 하도록
    //   되어 있음. (위 내용과 약간 다름)

    const uint8_t *input = m_inputBase + m_bufPos;
    uint8_t outputByte;

    if (m_bufPos == m_length && m_state < COBSENC_STATE_TRAILING_ZERO)
    {
        // 버퍼의 끝까지 다 처리했으면, 끝낸다.
        m_state = COBSENC_STATE_TRAILING_ZERO;
    }

    switch (m_state)
    {
    case COBSENC_STATE_LEADING_ZERO:
        outputByte = 0x00;
        m_state = COBSENC_STATE_NEW_SEQUENCE;
        break;
    case COBSENC_STATE_NEW_SEQUENCE:
    {
        int aheadLimit = min(254, m_length - m_bufPos);
        int i;
        for (i = 0; i < aheadLimit; i++)
        {
            if (input[i] == 0x00)
                break;
        }

        if (i == 0)
        {
            outputByte = 0x01;
            m_bufPos++;

            m_state = COBSENC_STATE_NEW_SEQUENCE;
        }
        else
        {
            outputByte = (uint8_t)(i + 1); // prefix code (이렇게 하면 0xFF인 case도 문제가 없다. 위의 aheadLimit 참조)
            m_thisTimeCopiedBytes = 0;
            m_thisTimeCopyingBytes = i;
            m_state = COBSENC_STATE_COPYING_BYTES;
        }
        break;
    }
    case COBSENC_STATE_COPYING_BYTES:
    {
        outputByte = input[m_thisTimeCopiedBytes++];
        if (m_thisTimeCopiedBytes == m_thisTimeCopyingBytes)
        {
            m_bufPos += m_thisTimeCopyingBytes + 1; // '1'은 0x00을 skip하기 위한 것임
            m_state = COBSENC_STATE_NEW_SEQUENCE;
        }
        break;
    }
    case COBSENC_STATE_TRAILING_ZERO:
        outputByte = 0x00;
        m_state = COBSENC_STATE_COMPLETE;
        break;
    default:
        return -1;
    }

    return outputByte;
}
bool COBSEncoder::IsEncodingComplete()
{
    return (m_state == COBSENC_STATE_COMPLETE);
}

