
#pragma once

typedef struct
{
    unsigned long upper;
    unsigned long lower;

} MICROSECOND64;

class Time
{
public:
    static unsigned long Micros();
    static MICROSECOND64 GetCurrentTime();
    static void UpdateRolloverCountQuantum();

private:
    static void UpdateRolloverCountQuantum(unsigned long timestamp);

private:
    static volatile unsigned long m_rolloverCount;
    static volatile unsigned long m_rolloverCheckLastTime; // 이전 값과 비교하여 rollover를 검출
};
