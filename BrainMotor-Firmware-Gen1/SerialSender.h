
#pragma once

#include <stdint.h>

#include "TaskBase.h"
#include "BocpDefinitions.h"
#include "COBSEncoder.h"

class SerialSender: public TaskBase
{
public:
    SerialSender();
    virtual ~SerialSender();

public:
    virtual void RunQuantum();
private:
    void SendSyncSigQuantum();
    void SendBodyQuantum();
    void GenerateSavePacket(const void *buf, int len, byte flags, byte seqNum);

public:
    // 전송 시작. 만약 현재 상태가 SS_STATE_READY가 아닌 경우, 전송에 실패함(false 반환).
    // 이때는 Reset()을 호출해야 함
    // * len: payload의 크기
    bool StartSending(const void *buf, int len, byte flags, byte seqNum, bool sendSyncSignal);
    bool IsSendComplete();
    void Reset();

private:
    uint8_t m_state;

    // Sync signal ('00') 관련
    bool m_bSendSyncSignal;
    int m_syncSignalSentBytes;

    // Packet (framed payload) 관련
    byte m_packet[BOCP_MAX_PACKET_SIZE];
    int m_packetLength;

    COBSEncoder m_cobsEncoder;
};

