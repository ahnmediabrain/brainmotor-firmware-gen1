
#include <Arduino.h>
#include "Time.h"

volatile unsigned long Time::m_rolloverCount = 0;
volatile unsigned long Time::m_rolloverCheckLastTime = 0;

// FIXME: 32비트 us단위 카운터의 오버플로우는 4294.967296초 = 1시간 12분 마다 일어나므로,
//        m_rolloverCount의 갱신에 있어서는 Polling을 할 필요가 없다;
//        사실은 GetCurrentTime을 호출할 때마다 확인하면 된다.
//        * GetCurrentTime이 호출되지 않는 경우에도 rollover count를 갱신하기는 해야 하지만,
//          이것은 1시간 12분의 시간동안 몇 번 정도만 하면 된다.  

unsigned long Time::Micros()
{
    return micros();
}
MICROSECOND64 Time::GetCurrentTime()
{
    MICROSECOND64 microsecond64;
    unsigned long timestamp;

    //ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        timestamp = micros();
        UpdateRolloverCountQuantum(timestamp);
        microsecond64.upper = m_rolloverCount;
    }

    microsecond64.lower = timestamp;
    return microsecond64;
}
void Time::UpdateRolloverCountQuantum()
{
    Time::UpdateRolloverCountQuantum(Time::Micros());
}
void Time::UpdateRolloverCountQuantum(unsigned long timestamp)
{
    if (timestamp < m_rolloverCheckLastTime)
    {
        // overflow
        m_rolloverCount++;
    }
    m_rolloverCheckLastTime = timestamp;
}
