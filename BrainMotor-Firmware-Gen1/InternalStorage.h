
#pragma once

#include <stdint.h>
#include "BocpDefinitions.h"

class InternalStorage
{
public:
    static void ReadSerialNumber(byte *buf); // buf: 16바이트 크기 필요 (시리얼 번호는 16글자 영문자/숫자임)
    static BocpStatusCode ReadSetting(byte settingCode, uint32_t *pOut); // 설정값 읽기
    static BocpStatusCode UpdateSetting(byte settingCode, uint32_t newValue); // 새로운 설정의 유효성 검사 및 갱신 (필요한 경우 다른 모듈에 통보)
    static uint8_t GetSettingCount(); // 설정값의 개수
};
