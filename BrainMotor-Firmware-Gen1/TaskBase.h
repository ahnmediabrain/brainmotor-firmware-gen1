
#pragma once

class TaskBase
{
public:
    TaskBase();
    virtual ~TaskBase();

public:
    virtual void RunQuantum() = 0;
};

