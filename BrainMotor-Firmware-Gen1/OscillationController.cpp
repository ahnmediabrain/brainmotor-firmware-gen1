
#include <Arduino.h>
#include "OscillationController.h"
#include "PWM.h"
#include "DeviceSpec.h"
#include "Time.h"
#include "BocpDefinitions.h"
#include "HdmDataQueue.h"
#include "LedModule.h"

#define PWOM_TIME_UNIT_MICROSEC 1024 // 마이크로초

class PinArrangement
{
public:
    // H-Bridge IC와 아두이노의 연결을 나타낸다. 실제 전선 연결에 따라 변경할 것.
    static const int Pulse_SIG = 11; // InitTimersSafe로 초기화한 PWM이 11번 pin에서만 작동하는 것으로 보임
    static const int Pulse_GND = 3;
    static const int Direction_SIG = 8;
    static const int Direction_GND = 6;
    static const int Offline_SIG = 5;
    static const int Offline_GND = 4;
};

OscillationController::OscillationController()
{
    m_currentOscillationMode = OSC_MODE_STOPPED;
    m_bMotorOn = false;
    m_plusDirection = DIRECTION_A;
    m_minusDirection = DIRECTION_B;

    m_hdmBufferUnderruns = 0;

    pinMode(PinArrangement::Pulse_SIG, OUTPUT);
    pinMode(PinArrangement::Pulse_GND, OUTPUT);
    pinMode(PinArrangement::Direction_SIG, OUTPUT);
    pinMode(PinArrangement::Direction_GND, OUTPUT);
    pinMode(PinArrangement::Offline_SIG, OUTPUT);
    pinMode(PinArrangement::Offline_GND, OUTPUT);

    UpdateMotorPowerState();
}
OscillationController::~OscillationController()
{
    Stop();
}

void OscillationController::RunQuantum()
{
    // OSC_MODE_STOPPED 또는 OSC_MODE_CONSTANT_VELOCITY 모드에서는
    // 지속적으로 수행할 작업이 없음 (전자의 경우 정말 아무것도 할 필요가 없고, 후자의
    // 경우에도 초기에 한 번만 주파수를 설정하면 됨)
    if (m_currentOscillationMode == OSC_MODE_HDM)// || m_currentOscillationMode == OSC_MODE_PREDEFINED_WAVEFORM)
    {
        MICROSECOND64 currentTime = Time::GetCurrentTime();

        bool tryUpdate;
        if (m_hdmIsUnderrun)
        {
            tryUpdate = true;
        }
        else
        {
            // 바로 이전 업데이트와의 시간차가 1024us가 넘는 경우에만 갱신 (그렇지 않다면 다음 번에 다시 확인)
            // -> 정확히 1024us가 되었을 때 갱신하게 됨
            unsigned long timestampDelta = currentTime.lower - m_hdmLastUpdatedTimeMicros;
            tryUpdate = (timestampDelta >= HDM_TIME_UNIT_MICROSECONDS); // FIXME: 시간차가 10us 이하인 경우 spinning wait를 구현할 것
        }
    
        if (!tryUpdate)
            return;
    
        // 만약에, buffer underrun이 일어났다고 치자. 그러면 어떻게 대처해야 하는가?
        // * currentTime은 계속 증가할 것이다. (현재 시간은 계속 갈 것이다)
        // * 그러나 버퍼의 현재 샘플은 안 증가할 것이다.
        // * 만약 버퍼링이 불필요하게 딜레이되는 경우, 망한다.
        //   (한 시간 딜레이됐다고 치면, 사용하지도 않을 한 시간치 데이터를 받아와야 한다)
        //   -> 그래서 HDM은 타임아웃이 0.5초이지 않는가?
        // * 0.5초 = 500 ms --> 500개의 샘플이면, 4*500 = 2000byte. protocol overhead 3*40% 고려해도 3000byte다.
        //                     can you keep up?
        // * 만약 underrun이 발생했다고 하자. 이론상 대역폭은 92.16Kbps이므로 실효 대역폭은 50Kbps라고 보면
        //   3000byte는 몇 초에 보낼 수 있겠는가? 24Kbit는 480ms에 보낼 수 있다.
        // * 250000의 baud rate를 쓴다면, 이론상 대역폭은 200Kbps이므로 실효 대역폭은 100Kbps라고 보면
        //   3000byte (24Kbit)는 240ms에 보낼 수 있다.
        // * 음... 성능이 정 안 나오면, 주파수 인코딩 방식을 바꿔서 16비트나 24비트로 만들자.
        //   어차피 PWM 주파수의 resolution은 똑바로 안 나온다. (32비트 resolution이 안 된다)
        //   => 16비트로 만들면, 1개 샘플당 2바이트뿐이고, 3000byte 대신 1500byte가 된다.
        //   => 12Kbit는 120ms에 보낼 수 있다. 따라서, 0.5초 drop이 발생하는 것은,
        //      처음으로 언더런이 발생한 시점으로부터 380ms가 지났음에도 아무 데이터도 전송되지 않았을 때이다.
        //      따라서, 이정도면 0.5초 타임아웃은 0.3초 정도의 실제 drop이 있을 때 발생할 수 있다.
        // * 일단 그냥 가자. 언더런이 발생하는 경우에는, 0.5초 이후에 어차피 drop되므로, 일단은 신경쓰지 말자.
        //   우리 목표는 언더런이 발생하지 않도록 버그를 수정하고 코드를 트윅하는 것이다.
        //   (Note: 언더런이 발생하고 0.5초 타임아웃이 지난 경우에는 HDM 모듈이 진동모듈을 정지상태로 전환한다)
        // * 가능한 개선사항: FIXME:
        //   - 언더런이 발생한 이후 데이터를 다시 수신하여 작동을 재개하는 경우, 이 RunQuantum loop에서 constant polling을 할 필요가 없고,
        //     대신 새롭게 데이터가 들어온 순간 HDM 모듈이 본 모듈로 통지해주는 방법을 생각할 수 있다.
    
        // FIXME:  시간이 되었을때 oscillation data 가져오는 것보다는
        //         미리 oscillation data는 가져다 놓고, 시간이 되면 새로 설정하고,
        //         그 다음 즉시 새로운 데이터를 준비해 놓는 방식으로 바꿔야 한다.
        uint32_t encodedFrequency;
        if (BOCP_FAILED(HdmTryGetOscillationData(&m_hdmDataQueue, currentTime, &encodedFrequency)))
        {
            if (!m_hdmIsUnderrun) m_hdmBufferUnderruns++;
            return;
        }
    
        SetMotorVelocity(encodedFrequency);
        m_hdmIsUnderrun = false;
        m_hdmLastUpdatedTimeMicros = (currentTime.lower & ~1023) | m_hdmTimeOffset;
    }
    else if (m_currentOscillationMode == OSC_MODE_PREDEFINED_WAVEFORM)
    {
        // Fill data by calculation
        // 바로 이전 업데이트와의 시간차가 1024us가 넘는 경우에만 갱신 (그렇지 않다면 다음 번에 다시 확인)
        // -> 정확히 1024us가 되었을 때 갱신하게 됨
        unsigned long currentTime = micros();
        long timestampDelta = (currentTime - m_pwomLastUpdatedTime);

        if (timestampDelta >= PWOM_TIME_UNIT_MICROSEC - 20)
        {
            // spin-wait
            if (timestampDelta < PWOM_TIME_UNIT_MICROSEC)
            {
                while (1)
                {
                    currentTime = micros();
                    timestampDelta = (currentTime - m_pwomLastUpdatedTime); // delta를 이용해야만 음수가 큰 양의 정수로 해석되는 것을 막을 수 있다.
                    if (timestampDelta >= PWOM_TIME_UNIT_MICROSEC)
                        break;
                }
            }

            // update
            SetMotorVelocity(m_pwomNextFreq);

            m_pwomLastUpdatedTime = (currentTime & ~1023) | m_pwomTimeOffset;

            unsigned long elapsedTime = m_pwomLastUpdatedTime - m_pwomTimeBase;
            if (elapsedTime >= 1000 * 1000 * 1000 /* 10억 마이크로초 = 1000초 */)
            {
                // reduce by period
                float periodInMicroseconds = 2 * PI * 1.0E6 / (m_pwomSineAngularFrequency);
                m_pwomTimeBase = m_pwomLastUpdatedTime - fmod(elapsedTime, periodInMicroseconds);
                m_pwomTimeOffset = (m_pwomTimeBase & 1023);
            }

            m_pwomNextFreq = CalcSinusoidalDriveFreq(m_pwomSineAmplitude, m_pwomSineAngularFrequency, m_pwomSinePhaseConstant, m_pwomLastUpdatedTime - m_pwomTimeBase + PWOM_TIME_UNIT_MICROSEC);
        }
    }
}

bool OscillationController::IsMotorPoweredOn()
{
    return m_bMotorOn;
}
BocpStatusCode OscillationController::PowerOnMotor()
{
    if (m_currentOscillationMode != OSC_MODE_STOPPED)
        return BOCP_E_INVALID_OPERATION;

    PowerOnMotorInternal();
    return BOCP_E_SUCCESS;
}
BocpStatusCode OscillationController::PowerOffMotor()
{
    if (m_currentOscillationMode != OSC_MODE_STOPPED)
        return BOCP_E_INVALID_OPERATION;

    PowerOffMotorInternal();
    return BOCP_E_SUCCESS;
}
void OscillationController::PowerOnMotorInternal()
{
    m_bMotorOn = true;
    UpdateMotorPowerState();
}
void OscillationController::PowerOffMotorInternal()
{
    m_bMotorOn = false;
    UpdateMotorPowerState();
}
void OscillationController::UpdateMotorPowerState()
{
    // Signal(HIGH) GND(LOW)로 설정하면 모터가 꺼짐(Offline). 이외의 조합에서는 모터가 켜짐.
    if (m_bMotorOn)
    {
        digitalWrite(PinArrangement::Offline_SIG, LOW);
        LedModule::On(LED_MOTOR_POWER);
    }
    else
    {
        digitalWrite(PinArrangement::Offline_SIG, HIGH);
        LedModule::Off(LED_MOTOR_POWER);
    }
    digitalWrite(PinArrangement::Offline_GND, LOW);
}

BocpStatusCode OscillationController::SetPlusDirection(byte direction)
{
    if (direction != DIRECTION_A && direction != DIRECTION_B)
        return BOCP_E_INVALID_PARAMETER;

    m_plusDirection = direction;
    if (direction == DIRECTION_A) m_minusDirection = DIRECTION_B;
    else m_minusDirection = DIRECTION_A;

    return BOCP_E_SUCCESS;
}
byte OscillationController::GetPlusDirection()
{
    return m_plusDirection;
}

OscillationMode OscillationController::GetCurrentOscillationMode()
{
    return m_currentOscillationMode;
}
BocpStatusCode OscillationController::FineControl(int32_t steps)
{
    if (m_currentOscillationMode != OSC_MODE_STOPPED)
        return BOCP_E_INVALID_OPERATION;

    bool fMotorPowered = IsMotorPoweredOn();
    if (!fMotorPowered)
    {
        PowerOnMotorInternal();
        delayMicroseconds(20000);
    }

    if (steps > 0)
    {
        SetMotorDirection(m_plusDirection);
    }
    else
    {
        SetMotorDirection(m_minusDirection);
    }

    uint32_t stepCount = (steps > 0) ? (steps) : (-steps);
    uint32_t delayTime = 500;
    for (uint32_t i = 0; i < stepCount; i++)
    {
        if (stepCount > 50)
        {
            if (i < 50)
            {
                delayTime = 300 - 5 * i;
            }
            else if (i > stepCount - 50)
            {
                delayTime = 300 - 5 * (stepCount - i);
            }
            else
            {
                delayTime = 50;
            }
        }

        // step manually
        digitalWrite(PinArrangement::Pulse_SIG, 255);
        delayMicroseconds(delayTime);
        digitalWrite(PinArrangement::Pulse_SIG, 0);
        delayMicroseconds(delayTime);
    }

    if (!fMotorPowered)
    {
        PowerOffMotorInternal();
    }
    return BOCP_E_SUCCESS;
}
BocpStatusCode OscillationController::StartConstantVelocityMode(uint32_t encodedFrequency)
{
    m_currentOscillationMode = OSC_MODE_CONSTANT_VELOCITY;

    // FIXME: implement range check

    m_cvmEncodedFrequency = encodedFrequency;
    SetMotorVelocity(m_cvmEncodedFrequency);
    PowerOnMotorInternal();

    return BOCP_E_SUCCESS;
}
BocpStatusCode OscillationController::StartSinusoidalDrive(float amplitude, float angularFrequency, float phaseConstant)
{
    m_pwomWaveformType = WAVEFORM_SINUSOIDAL;
    m_pwomSineAmplitude = amplitude;
    m_pwomSineAngularFrequency = angularFrequency;
    m_pwomSinePhaseConstant = phaseConstant;

    //uint32_t timestampFinish = micros() + 1000000;
    this->FineControl((int32_t)m_pwomSineAmplitude);
    //while (micros() < timestampFinish);

    m_currentOscillationMode = OSC_MODE_PREDEFINED_WAVEFORM;

    // 시작점으로부터의 시간도 저장. * note: 시간이 무한정 증가할 수는 없으므로 특정 주기가 넘어가면 기준시간에 얼마를 더한다.
    uint32_t freq = CalcSinusoidalDriveFreq(m_pwomSineAmplitude, m_pwomSineAngularFrequency, m_pwomSinePhaseConstant, 0);
    PowerOnMotorInternal();
    SetMotorVelocity(freq);

    m_pwomTimeBase = micros();
    m_pwomLastUpdatedTime = m_pwomTimeBase;
    m_pwomTimeOffset = (m_pwomLastUpdatedTime & 1023);

    m_pwomNextFreq = CalcSinusoidalDriveFreq(m_pwomSineAmplitude, m_pwomSineAngularFrequency, m_pwomSinePhaseConstant, PWOM_TIME_UNIT_MICROSEC);

    return BOCP_E_SUCCESS;
}
//BocpStatusCode OscillationController::StartHostDrivenMode(HDM_DATA_QUEUE *pHdmDataQueue)
//{
//    if (pHdmDataQueue == NULL)
//        return;
//
//    // NOTE: 실제 진동은 RunQuantum에서 수행함.
//    m_currentOscillationMode = OSC_MODE_HDM;
//    m_pHdmDataQueue = pHdmDataQueue;
//    m_pHdmDataQueue->timestampBase = Time::GetCurrentTime();
//    m_hdmTimeOffset = m_pHdmDataQueue->timestampBase & 1023;
//    m_hdmIsUnderrun = true; // 이렇게 설정해야 RunQuantum에서 데이터를 즉각 갱신함
//    PowerOnMotor();
//}
BocpStatusCode OscillationController::Stop()
{
    PowerOffMotorInternal();
    SetMotorVelocity(0);
    m_currentOscillationMode = OSC_MODE_STOPPED;

    return BOCP_E_SUCCESS;
}

BocpStatusCode OscillationController::HdmPrepare()
{
    if (m_currentOscillationMode != OSC_MODE_STOPPED)
        return BOCP_E_INVALID_OPERATION;

    m_currentOscillationMode = OSC_MODE_HDM;
    m_hdmState = HDM_STATE_INITIAL_BUFFERING;

    // HDM Queue를 재설정.

    return BOCP_E_SUCCESS;
}
BocpStatusCode OscillationController::HdmSupplyInitialBufferingData()
{
    return BOCP_E_SUCCESS;
}
BocpStatusCode OscillationController::HdmSupplyData()
{
    return BOCP_E_SUCCESS;
}
BocpStatusCode OscillationController::HdmStop()
{
    return BOCP_E_SUCCESS;
}

void OscillationController::SetMotorVelocity(uint32_t encodedFrequency)
{
    uint32_t frequencyAbsValue = BOCP_FREQ_ABS_INT_PART(encodedFrequency);
    byte direction = (encodedFrequency & 0x80000000) ? m_plusDirection : m_minusDirection;

    SetMotorFrequency(frequencyAbsValue);
    SetMotorDirection(direction);
}
void OscillationController::SetMotorFrequency(uint32_t frequencyAbsValue)
{
    bool bFrequencyWasSet = SetPinFrequencySafe(PinArrangement::Pulse_SIG, frequencyAbsValue);
    if (bFrequencyWasSet)
    {
        pwmWrite(PinArrangement::Pulse_SIG, 127); // Half duty cycle (127/255)
    }
    else
    {
        // 설정해야 하는 주파수를 설정하지 못했다면, 그냥 PWM을 끈다.
        // 왜냐하면 이것은 주파수가 너무 작거나 너무 커서 PWM을 설정할 수 없음을 의미하기 때문이다.
        pwmWrite(PinArrangement::Pulse_SIG, 0);
    }
    digitalWrite(PinArrangement::Pulse_GND, 0);
}
void OscillationController::SetMotorDirection(byte direction)
{
    if (direction == DIRECTION_A)
    {
        digitalWrite(PinArrangement::Direction_SIG, HIGH);
    }
    else
    {
        digitalWrite(PinArrangement::Direction_SIG, LOW);
    }
    digitalWrite(PinArrangement::Direction_GND, LOW);
}

uint32_t OscillationController::CalcSinusoidalDriveFreq(float a, float w, float p, uint32_t t)
{
    float periodInMicroseconds = 2 * PI * 1.0E6 / (m_pwomSineAngularFrequency);
    t = fmod(t, periodInMicroseconds);

    float phase = w * (t / 1.0E6) + p;
    float stepsPerSecond = - w * a * sin(phase);

    uint32_t freq = 0;
    if (stepsPerSecond > 0) freq |= 0x80000000; // 부호 비트

    freq |= (uint32_t)(fabs(stepsPerSecond) * 256) & 0x7FFFFFFF;
    return freq;
}

