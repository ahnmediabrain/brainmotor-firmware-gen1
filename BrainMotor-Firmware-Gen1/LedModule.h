
#pragma once

#define LED_ARDUINO_POWER       0x0001
#define LED_MOTOR_POWER         0x0002
#define LED_OSCILLATION_STATE   0x0004
#define LED_HOST_CONNECTION     0x0008

class LedModule
{
public:
    static void Initialize();
    static void On(uint16_t pin);
    static void Off(uint16_t pin);
    static void UpdatePinVoltage(); // 현재 LED On/Off 정보를 핀의 전압에 재설정

private:
    static uint16_t m_led;
};

