
#ifndef _CRC16_CCITT_H_
#define _CRC16_CCITT_H_

uint16_t Crc16_CCITT(const void *buf, int len);

#endif /* _CRC16_H_ */
