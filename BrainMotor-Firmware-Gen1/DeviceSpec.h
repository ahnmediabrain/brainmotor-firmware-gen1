
#pragma once

#define PULSE_PER_REVOLUTION 1600
#define DISTANCE_PER_REVOLUTION 8000 // 마이크로미터 단위

// [1024us로 하는 이유]
// ATmega2560의 클럭이 16MHz이므로 prescaler를 64로
// 설정하면 정확히 4us 간격의 타이머를 얻을 수 있기 때문이다.
// 그러므로, micros()는 division을 수행할 필요가 없다;
// 그냥 반환하면 된다.
#define HDM_TIME_UNIT_MICROSECONDS 1024
#define HDM_TIME_UNIT_NANOSECONDS (HDM_TIME_UNIT_MICROSECONDS * 1000UL)

#define HDM_BUFFER_CAPACITY 400

