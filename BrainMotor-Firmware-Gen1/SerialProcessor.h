
#pragma once

#include "BocpDefinitions.h"

class SerialProcessor
{
public:
    SerialProcessor();
    ~SerialProcessor();

public:
    // 메시지가 처리되었으면 true 반환. 실패했으면 false 반환.
    // (NOTE 1: 메시지가 올바르지 않은 경우에는 BOCP 스펙에 따라
    //          오류 코드를 포함하는 응답이 생성된다)
    // (NOTE 2: 이전에 처리된 메시지에 대한 응답이 생성되었으나 처리되었다는 통보가 없다면,
    //          이번 호출 시 이전 메시지에 대한 응답은 삭제되고 새로운 응답이 내부 버퍼에 쓰여진다.
    //          그러나, 이전 응답은 RT Flag 설정된 패킷에 대해서는 보존된다)
    // (NOTE 3: out 버퍼는 BOCP_MAX_PAYLOAD_SIZE 크기의 데이터를 담기에 충분해야 한다)
    void ProcessMessage(const void *in_void, size_t len, void *out_void, size_t *written);
    void ConnectionEstablished();
    void ConnectionEnded();
    void ConnectionReset();

private:
    void WriteCommonErrorResponse(BocpStatusCode statusCode, byte requestMsgType, byte *out, size_t *written);

private:
    // 아래 함수들은, in 버퍼는 MsgType을 포함하며, out 버퍼는 MsgType부터 쓰면 되는 위치부터임.
    void ProcessEcho1(const byte *in, size_t len, byte *out, size_t *written);
    void ProcessTimeSync1(const byte *in, size_t len, byte *out, size_t *written);
    void ProcessSpecQuery1(const byte *in, size_t len, byte *out, size_t *written);
    void ProcessStateQuery1(const byte *in, size_t len, byte *out, size_t *written);
    void ProcessSettingQuery1(const byte *in, size_t len, byte *out, size_t *written);
    void ProcessSettingUpdate1(const byte *in, size_t len, byte *out, size_t *written);
    void ProcessFineControl1(const byte *in, size_t len, byte *out, size_t *written);
    void ProcessConstantVelocityMove1(const byte *in, size_t len, byte *out, size_t *written);
    void ProcessStopOscillation1(const byte *in, size_t len, byte *out, size_t *written);
    void ProcessPredefinedWaveformOscillationMode1(const byte *in, size_t len, byte *out, size_t *written);
    void ProcessHdmPrepare1(const byte *in, size_t len, byte *out, size_t *written);
    void ProcessHdmInitialBufferingReq(const byte *in, size_t len, byte *out, size_t *written);
    void ProcessHdmOscData(const byte *in, size_t len, byte *out, size_t *written);
    void ProcessHdmStop1(const byte *in, size_t len, byte *out, size_t *written);
    void ProcessControlMotorPower1(const byte *in, size_t len, byte *out, size_t *written);
};

